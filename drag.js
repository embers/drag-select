
(function(){
   //事件处理程序
   //elem DOM对象  eventName 事件名称  eventType 事件类型
   function eventHandler(elem, eventName, eventType){
       // elem.attachEvent 兼容IE9以下事件
       elem.addEventListener ? elem.addEventListener(eventType, eventName, false) : elem.attachEvent('on'+eventType, eventName);
    };
    //移除事件兼容处理
    function removeEventHandler(elem, eventName, eventType){
        elem.removeEventListener ? elem.removeEventListener(eventType, eventName) : elem.detachEvent(eventType, eventName);
    }
    //获取style属性值
    function getStyleValue(elem, property){
        //getComputedStyle、currentStyle 返回CSS样式声明对象([object CSSStyleDeclaration]) 只读
        //getComputedStyle 支持IE9+以上及正常浏览器
        //currentStyle 兼容IE8及IE8以下获取目标元素style样式
        return window.getComputedStyle(elem,null) ? window.getComputedStyle(elem,null)[property] : elem.currentStyle[property];
    }
    //被拖拽构造函数
    function Drag(selector){
        //elem DOM对象
        this.elem = typeof selector === 'object' ? selector : document.getElementById(selector);
        //元素大小
        this.offsetWidth=0
        this.offsetHeight=0
        //元素初始化位置
        this.initObjX = 0;
        this.initObjY = 0;
        //鼠标初始化位置
        this.initMouseX = 0;
        this.initMouseY = 0;
        this.isDraging = false;

        //父盒子的宽高
        this.pWidth = 0; //父盒子的宽度
        this.pHeight =0;

        //初始化--鼠标事件操作
        this._init();


    }
    //Drag对象原型
    Drag.prototype = {
        constructor : Drag,  
        //初始化
        //构造原型指回Drag 等价于==>>Drag.prototype._init = function(){}
        //初始化鼠标事件及鼠标操作流程
        _init : function(){ 
            this.setDrag();
        },
        //获取目标元素pos位置
        getObjPos : function(elem) {
            var pos = {x: 0, y: 0};
            if(getStyleValue(elem, 'position') == 'relative') {
                this.elem.style.position = 'absolute';
                return pos;
            } else {
                var x = parseInt(getStyleValue(elem, 'left') ? getStyleValue(elem, 'left') : 0);
                var y = parseInt(getStyleValue(elem, 'top') ? getStyleValue(elem, 'top') : 0);

                return pos = {
                    x: x,
                    y: y
                }
            }
        },
        //设置被拖动元素的位置
        setObjPos : function (elem, pos){
            elem.style.position = 'absolute';
            elem.style.left = pos.x+'px';
            elem.style.top = pos.y+'px';
        },
        //设置目标元素事件及操作流程
        setDrag : function(){
            //目标元素对象
            var self =  this;
            var time = null; //定时器

             function mousedown(event){
                event = window.event || event;

                //当前元素的大小
                this.offsetWidth=event.offsetWidth
                this.offsetHeight=event.offsetHeight


                //鼠标按下时位置
                this.initMouseX = event.clientX;
                this.initMouseY = event.clientY;
                //获取元素初始化位置pos
                var pos = self.getObjPos(self.elem);
                this.initObjX = pos.x;
                this.initObjY = pos.y;
                
                  this.pWidth = self.elem.parentElement.offsetWidth; //父盒子的宽度
                  this.pHeight  = self.elem.parentElement.offsetHeight;
                //mousemove
                time = setTimeout(function(){ //缓解移动卡顿
                    eventHandler(self.elem, mousemove, "mousemove");
                }, 25);
                //mouseup
                eventHandler(self.elem, mouseup, "mouseup");
                //按下标识
                self.isDraging = true;
            }
            function mousemove(event){
                event = window.event || event;
                if(self.isDraging){
                    //元素移动位置 == 当前鼠标移动位置 - 鼠标按下位置 + 目标元素初始化位置 

                    var moveX = event.clientX - this.initMouseX +  this.initObjX;
                    var moveY =  event.clientY - this.initMouseY +  this.initObjY;

                    let resW = this.pWidth -  this.offsetWidth; //父盒子宽减去移动盒子的宽 = 最大可移动的区域
                    let resH = this.pHeight -  this.offsetHeight; //父盒子高减去移动盒子的高 = 可最大移动的区域
                    moveX = moveX >= resW ? resW : moveX <= 0 ? 0 : moveX;
                    moveY = moveY >= resH ? resH : moveY <= 0 ? 0 : moveY;
                    //设置拖拽元素位置
                    self.setObjPos(self.elem, {
                        x : moveX,
                        y : moveY,
                    });
                    
                 }
             }
             function mouseup(event){
                 event = window.event || event;
                 self.isDraging = false;
                 clearTimeout(time);
                 //移除事件
                 removeEventHandler(document, mousemove, 'mousemove');
                 removeEventHandler(document, mouseup, 'mouseup');
             }
             //mousedown
             eventHandler(this.elem, mousedown, "mousedown");
         }
     }
     //将Drag挂到全局对象window上
     window.Drag = Drag;
 })();